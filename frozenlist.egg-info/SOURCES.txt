.coveragerc
CHANGES.rst
CONTRIBUTORS.txt
LICENSE
MANIFEST.in
README.rst
pyproject.toml
pytest.ini
setup.cfg
docs/Makefile
docs/changes.rst
docs/conf.py
docs/index.rst
docs/make.bat
docs/spelling_wordlist.txt
docs/contributing/guidelines.rst
docs/contributing/release_guide.rst
frozenlist/__init__.py
frozenlist/__init__.pyi
frozenlist/_frozenlist.pyx
frozenlist/py.typed
frozenlist.egg-info/PKG-INFO
frozenlist.egg-info/SOURCES.txt
frozenlist.egg-info/dependency_links.txt
frozenlist.egg-info/not-zip-safe
frozenlist.egg-info/top_level.txt
packaging/README.md
packaging/pep517_backend/__init__.py
packaging/pep517_backend/__main__.py
packaging/pep517_backend/_backend.py
packaging/pep517_backend/_compat.py
packaging/pep517_backend/_cython_configuration.py
packaging/pep517_backend/_transformers.py
packaging/pep517_backend/cli.py
packaging/pep517_backend/hooks.py
requirements/ci.txt
requirements/dev.txt
requirements/doc.txt
requirements/towncrier.txt
tests/conftest.py
tests/test_frozenlist.py